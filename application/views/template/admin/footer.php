</main>
<!--Main layout-->

<!--Footer-->
<footer class="page-footer text-center font-small primary-color-dark darken-2 mt-4 wow fadeIn fixed-bottom">
    <!--Copyright-->
    <div class="footer-copyright py-3">
        © 2020 Copyright:
        <a href="https://gitlab.com/Chusfiandi/" target="_blank"> Chusfiandi</a>
    </div>
    <!--/.Copyright-->

</footer>
<!--/.Footer-->

<!-- SCRIPTS -->
<script>
$("#user").text("TOKO:" + sessionStorage.getItem("username"));
// logout
$("#logout").click(function(e) {
    sessionStorage.removeItem("username");
    sessionStorage.removeItem("level");
    window.location.href = "http://localhost/pointsale-client/auth";
    const Toast = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener("mouseenter", Swal.stopTimer);
            toast.addEventListener("mouseleave", Swal.resumeTimer);
        },
    });

    Toast.fire({
        icon: "success",
        title: "Logout in successfully",

    });

});
</script>
<!-- validate -->
<script type="text/javascript" src="<?= base_url('assets/app/') ?>jquery.validate.js"></script>
<!-- main js -->
<script type="text/javascript" src="<?= base_url('assets/app/') ?>main.js"></script>
<!-- JQuery -->
<script type="text/javascript" src="<?= base_url('assets/admin/') ?>js/jquery-3.4.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="<?= base_url('assets/admin/') ?>js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="<?= base_url('assets/admin/') ?>js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="<?= base_url('assets/admin/') ?>js/mdb.min.js"></script>
<!-- sweet alert -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<!-- Initializations -->
<script type="text/javascript">
// Animations initialization
new WOW().init();
</script>
</body>

</html>