    <!--Main Navigation-->
    <header>

        <!-- Navbar -->
        <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
            <div class="container-fluid">

                <!-- Brand -->
                <a class="navbar-brand waves-effect" href="#" target="_blank">
                    <strong class="blue-text" id="user"></strong>
                </a>

                <!-- Collapse -->
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!-- Links -->
                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                    <!-- Left -->
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link waves-effect" href="#">Home
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <!--   <li class="nav-item">
                            <a class="nav-link waves-effect" href="https://mdbootstrap.com/docs/jquery/"
                                target="_blank">About
                                MDB</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link waves-effect"
                                href="https://mdbootstrap.com/docs/jquery/getting-started/download/"
                                target="_blank">Free
                                download</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link waves-effect" href="https://mdbootstrap.com/education/bootstrap/"
                                target="_blank">Free
                                tutorials</a>
                        </li> -->
                    </ul>

                    <!-- Right -->
                    <ul class="navbar-nav nav-flex-icons">
                        <li class="nav-item">
                            <a href="" class="nav-link waves-effect">

                            </a>
                        <li class="nav-item">
                            <a href="" class="nav-link  rounded waves-effect" id="logout">
                                Logout<i class="fas fa-sign-out-alt"></i>
                            </a>
                        </li>
                    </ul>

                </div>

            </div>
        </nav>
        <!-- Navbar -->
        <script>
        $("#user").text(sessionStorage.getItem("username"));
        // logout
        $("#logout").click(function(e) {
            sessionStorage.removeItem("username");
            sessionStorage.removeItem("level");

            const Toast = Swal.mixin({
                toast: true,
                position: "top-end",
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: (toast) => {
                    toast.addEventListener("mouseenter", Swal.stopTimer);
                    toast.addEventListener("mouseleave", Swal.resumeTimer);
                },
            });

            Toast.fire({
                icon: "success",
                title: "Logout in successfully",

            });
            window.location.href = "http://localhost/pointsale-client/home";
        });
        </script>

        <!-- Sidebar -->
        <div class="sidebar-fixed position-fixed">

            <a class="logo-wrapper waves-effect">
                <img src="https://mdbootstrap.com/img/logo/mdb-email.png" class="img-fluid" alt="">
            </a>

            <div class="list-group list-group-flush ">
                <a href="<?= base_url('admin/admin/gift'); ?>"
                    class="list-group-item list-group-item-action waves-effect"><i class="fas fa-gift pink-text mr-3"
                        aria-hidden="true"></i>
                    Hadiah</a>
                <a href="<?= base_url('admin/admin'); ?>" class="list-group-item list-group-item-action waves-effect">
                    <i class="fas fa-table  mr-3"></i>Produk</a>
                <a href="<?= base_url('admin/admin/alltransaksi'); ?>"
                    class="list-group-item list-group-item-action waves-effect">
                    <i class="fas fa-money-bill-alt green-text mr-3"></i>Transaksi</a>
            </div>

        </div>
        <!-- Sidebar -->

    </header>
    <!--Main Navigation-->
    <main class="pt-5 mx-lg-5">