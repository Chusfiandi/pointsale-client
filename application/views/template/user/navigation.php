 <!-- Navbar -->
 <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
     <div class="container">

         <!-- Brand -->
         <a class="navbar-brand waves-effect" href="https://majapahit.id/" target="_blank">
             <strong class="blue-text">MJP</strong>
         </a>

         <!-- Collapse -->
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
             aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
             <span class="navbar-toggler-icon"></span>
         </button>

         <!-- Links -->
         <div class="collapse navbar-collapse" id="navbarSupportedContent">

             <!-- Left -->
             <ul class="navbar-nav mr-auto">
                 <li class="nav-item active">
                     <a class="nav-link waves-effect" href="#">Home
                         <span class="sr-only">(current)</span>
                     </a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link waves-effect" href="https://gitlab.com/chusfiandi/" target="_blank">About
                         me</a>
                 </li>
             </ul>

             <!-- Right -->
             <ul class="navbar-nav nav-flex-icons">
                 <li class="nav-item">
                     <a href="https://www.facebook.com/chusfiandi.farid" class="nav-link waves-effect" target="_blank">
                         <i class="fab fa-facebook-f"></i>
                     </a>
                 </li>
                 <li class="nav-item">
                     <a href="" class="nav-link border border-light rounded waves-effect" data-toggle="modal"
                         data-target="#modalLoginForm"><i class="fas fa-sign-in-alt"></i> Login</a>
                 </li>
                 <li class="nav-item">
                     <a href="" class="nav-link border border-light rounded waves-effect" data-toggle="modal"
                         data-target="#modalRegisterForm"> <i class="fas fa-user-plus"></i>Register</a>
                 </li>
             </ul>

         </div>

     </div>
 </nav>
 <!-- Navbar -->
 <main>