 <!-- Navbar -->
 <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
     <div class="container">

         <!-- Brand -->
         <a class="navbar-brand waves-effect" href="https://mdbootstrap.com/docs/jquery/" target="_blank">
             <strong class="blue-text">MDB</strong>
         </a>

         <!-- Collapse -->
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
             aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
             <span class="navbar-toggler-icon"></span>
         </button>

         <!-- Links -->
         <div class="collapse navbar-collapse" id="navbarSupportedContent">

             <!-- Left -->
             <ul class="navbar-nav mr-auto">
                 <li class="nav-item">
                     <a class="nav-link waves-effect" href="<?= base_url('customer/myreward') ?>">
                         My Reward</a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link waves-effect" href="<?= base_url('customer') ?>">
                         Produk</a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link waves-effect" href="<?= base_url('customer/exchange') ?>">
                         Hadiah<i class="fas fa-gift pink-text" aria-hidden="true"></i></a>
                 </li>
             </ul>

             <!-- Right -->
             <ul class="navbar-nav nav-flex-icons">
                 <li class="nav-item">
                     <a href="" class="nav-link waves-effect" data-toggle="modal" data-target="#" id="kupoin">
                         MyPoint
                     </a>
                 </li>
                 <li class="nav-item">
                     <a href="<?= base_url('customer/histori') ?>" class="nav-link waves-effect">
                         <i class="fas fa-history"></i>Histori Transaksi
                     </a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link waves-effect " data-toggle="modal" data-target="#modalCart" id="cart">
                         <i class="fas fa-shopping-cart"></i>
                         <span class="clearfix d-none d-sm-inline-block"> Cart </span>
                     </a>
                 </li>
                 <li class="nav-item">
                     <a href="" class="nav-link waves-effect" id="logout">
                         <i class="fas fa-sign-out-alt"></i>Logout
                     </a>
                 </li>
             </ul>

         </div>

     </div>
 </nav>
 <!-- Navbar -->
 <main class="mt-5 pt-3">
     <!-- Modal -->
     <div class="modal fade" id="poinku" tabindex="-1" role="dialog" aria-labelledby="poinku" aria-hidden="true"
         id="Dpoin">
         <div class="modal-dialog" role="document">
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 class="modal-title" id="poinkuisi">MY Point</h5>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
                 </div>
                 <div class="modal-body" id="isipoin">
                     <p></p>
                 </div>
                 <div class="modal-footer">
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                 </div>
             </div>
         </div>
     </div>


     <!-- Modal: modalCart -->
     <div class="modal fade" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
         <div class="modal-dialog" role="document">
             <div class="modal-content">
                 <!--Header-->
                 <div class="modal-header" id="modalCart">
                     <h4 class="modal-title" id="myModalLabel">Your cart</h4>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">×</span>
                     </button>
                 </div>
                 <!--Body-->
                 <div class="modal-body">

                     <table class="table table-hover">
                         <thead>
                             <tr>
                                 <th>#</th>
                                 <th>Product name</th>
                                 <th>Price</th>
                                 <th>qty</th>
                                 <th>total</th>
                                 <th>Action</th>
                             </tr>
                         </thead>
                         <tbody id="cartisi">


                         </tbody>
                     </table>

                 </div>
                 <!--Footer-->
                 <div class="modal-footer">
                     <button type="button" class="btn btn-outline-primary" data-dismiss="modal"
                         id="closeC">Close</button>
                     <!-- <button class="btn btn-primary" id="bayar">Checkout</button> -->
                 </div>
             </div>
         </div>
     </div>
     <!-- Modal: modalCart -->