</main>
<!--Main layout-->

<!--Footer-->
<footer class="page-footer text-center font-small mt-4 wow fadeIn fixed-bottom">

    <!-- <hr class="my-4"> -->

    <!--Copyright-->
    <div class="footer-copyright py-3">
        © 2019 Copyright:
        <a href="gitlab.com/chusfiandi" target="_blank">chusfiandi</a>
    </div>
    <!--/.Copyright-->

</footer>
<!--/.Footer-->

<!-- SCRIPTS -->
<!-- app -->
<script src="<?= base_url('assets/app/auth.js'); ?>"></script>
<script src="<?= base_url('assets/app/cart.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/app/') ?>main.js"></script>
<!-- JQuery -->
<script type="text/javascript" src="<?= base_url('assets/user'); ?>/js/jquery-3.4.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="<?= base_url('assets/user'); ?>/js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="<?= base_url('assets/user'); ?>/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="<?= base_url('assets/user'); ?>/js/mdb.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<!-- Initializations -->
<script type="text/javascript">
// Animations initialization
new WOW().init();
</script>
</body>

</html>