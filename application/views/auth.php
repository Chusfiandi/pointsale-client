<div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Sign up</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="md-form mb-5">
                    <i class="fas fa-user prefix grey-text"></i>
                    <input type="text" id="orangeForm-name" class="form-control validate" name="name">
                    <label data-error="wrong" data-success="right" for="orangeForm-name">Your name</label>
                </div>
                <div class="md-form mb-5">
                    <i class="fas fa-user prefix grey-text"></i>
                    <input type="text" id="orangeForm-username" class="form-control validate" name="username">
                    <label data-error="wrong" data-success="right" for="orangeForm-name">Your username</label>
                </div>
                <div class="md-form mb-5">
                    <i class="fas fa-envelope prefix grey-text"></i>
                    <input type="email" id="orangeForm-email" class="form-control validate" name="email">
                    <label data-error="wrong" data-success="right" for="orangeForm-email">Your Email</label>
                </div>

                <div class="md-form mb-4">
                    <i class="fas fa-lock prefix grey-text"></i>
                    <input type="password" id="orangeForm-pass" class="form-control validate" name="password">
                    <label data-error="wrong" data-success="right" for="orangeForm-pass">Your password</label>
                </div>

            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button class="btn btn-deep-orange" id="register">Sign up</button>
            </div>
        </div>
    </div>
</div>
<!-- Material form logi n -->
<div class="row d-flex justify-content-md-center mt-5 align-content-center">
    <div class="card col-6">

        <h5 class="card-header info-color white-text text-center py-4">
            <strong>Sign in</strong>
        </h5>

        <!--Card content-->
        <div class="card-body px-lg-5 pt-0">

            <!-- Form -->
            <form class="text-center" style="color: #757575;" action="#!" id="formLogin">

                <!-- Email -->
                <div class="md-form">
                    <input type="email" id="email" class="form-control" name="username">
                    <label for="email">E-mail/username</label>
                </div>

                <!-- Password -->
                <div class="md-form">
                    <input type="password" id="password" class="form-control" name="password">
                    <label for="password">Password</label>
                </div>

                <div class="d-flex justify-content-around">
                    <div>
                        <!-- Remember me -->
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="materialLoginFormRemember">
                            <label class="form-check-label" for="materialLoginFormRemember">Remember me</label>
                        </div>
                    </div>
                    <div>
                        <!-- Forgot password -->
                        <a href="">Forgot password?</a>
                    </div>
                </div>

                <!-- Sign in button -->
                <button class="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit"
                    id="login">Sign
                    in</button>

                <!-- Register -->
                <p>Not a member?
                    <a href="" data-toggle="modal" data-target="#modalRegisterForm">Register</a>
                </p>

            </form>
            <!-- Form -->

        </div>

    </div>
</div>
<!-- Material form login -->
<script>
$(document).ready(function() {
    $("#register").click(function(e) {
        // console.log("oke");
        // return;
        e.preventDefault();
        var form = new FormData();
        form.append(
            "username",
            $('#modalRegisterForm input[name = "username"]').val()
        );
        form.append(
            "email",
            $('#modalRegisterForm input[name = "email"]').val()
        );
        form.append("name", $('#modalRegisterForm input[name = "name"]').val());
        form.append(
            "password",
            $('#modalRegisterForm input[name = "password"]').val()
        );

        var settings = {
            url: "" + rest.url + "/user/register",
            method: "POST",
            timeout: 0,
            processData: false,
            mimeType: "multipart/form-data",
            contentType: false,
            data: form,
        };

        $.ajax(settings).done(function(response) {
            var responseText = JSON.parse(response);
            if (responseText.status == false) {
                Swal.fire({
                    icon: "error",
                    title: "Gagal",
                    text: responseText.message,
                });
            } else {
                const Toast = Swal.mixin({
                    toast: true,
                    position: "top-end",
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener("mouseenter", Swal.stopTimer);
                        toast.addEventListener("mouseleave", Swal.resumeTimer);
                    },
                });

                Toast.fire({
                    icon: "success",
                    title: "Register in successfully",
                });
            }
            // $("#modalRegisterForm ").toggle();
            // return false;
        });
    });
    // login
    $("#login").click(function(e) {
        e.preventDefault();
        var form = new FormData();
        form.append(
            "username",
            $('#formLogin input[name = "username"]').val()
        );
        form.append(
            "password",
            $('#formLogin input[name = "password"]').val()
        );

        var settings = {
            url: "" + rest.url + "/user/login",
            method: "POST",
            timeout: 0,
            processData: false,
            mimeType: "multipart/form-data",
            contentType: false,
            data: form,
        };

        $.ajax(settings).done(function(response) {
            var responseText = JSON.parse(response);
            var data = responseText.data;
            if (responseText.status == false) {
                Swal.fire({
                    icon: "error",
                    title: "Gagal",
                    text: responseText.message,
                });
            } else {
                // console.log(data);
                // return;
                const Toast = Swal.mixin({
                    toast: true,
                    position: "top-end",
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener("mouseenter", Swal.stopTimer);
                        toast.addEventListener("mouseleave", Swal.resumeTimer);
                    },
                });
                Toast.fire({
                    icon: "success",
                    title: "Login in successfully",
                });
                setTimeout(function() {
                    sessionStorage.setItem("username", data.username);
                    sessionStorage.setItem("level", data.level);
                    window.location.href =
                        "http://localhost/pointsale-client/admin/admin";
                }, 3000);
            }
        });
    });

});
</script>
<script>
try {
    var level = sessionStorage.getItem("level");
    if (level == "admin") {
        window.location.href = "http://localhost/pointsale-client/admin/admin";
    }
} catch {
    console.error();
}
</script>