<script>
try {
    var level = sessionStorage.getItem("level");
    if (level != "customer") {
        window.location.href = "http://localhost/pointsale-client/home";
    }
} catch {
    console.error();
}
</script>
<section class="mt-3">
    <div class="row">
        <div class="container">
            <table class="table table-hover text-center">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama produk</th>
                        <th scope="col">Harga produk</th>
                        <th scope="col">Diskon produk</th>
                        <th scope="col">Qty produk</th>
                        <th scope="col">Total harga</th>
                        <th scope="col">Tanggal Transaksi</th>
                    </tr>
                </thead>
                <tbody id="contentH">

                </tbody>
            </table>
        </div>
    </div>
</section>
<script>
$(document).ready(function() {
    getDataProduct();

    function getDataProduct() {
        var form = new FormData();
        var settings = {
            url: "" + rest.url + "/api/transaksi/histori?id_customer=" + sessionStorage
                .getItem(
                    'id') + "",
            method: "GET",
            timeout: 0,
            headers: {
                Authorization: rest.token,
            },
        };

        $.ajax(settings).done(function(response) {
            dataP = response.data;
            // console.log(response.data);
            // return;
            $(".remove").remove();
            $.each(dataP, function(i, data) {
                $("#contentH").append(
                    `
	                <tr class="remove">
	                    <th scope="row">${i + 1}</th>
	                    <td>${data.name_product}</td>
						<td>${data.price_product}</td>
						<td>${data.diskon}%</td>
	                    <td>${data.qty}</td>
                        <td>${data.total}</td>
                        <td>${data.created_at}</td>
	                </tr>
	            `
                );
            });
        });
    }
});
</script>