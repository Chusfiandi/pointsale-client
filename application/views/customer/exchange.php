    <!-- Heading -->
    <h2 class="my-5 h2 text-center">Tukar Hadaih</h2>
    <section id="daftar_produk" class="text-center mb-4">
        <div class="row wow fadeIn" id="list_produk">

        </div>
    </section>
    <script>
try {
    var level = sessionStorage.getItem("level");
    if (level != "customer") {
        window.location.href = "http://localhost/pointsale-client/home";
    }
} catch {
    console.error();
}
    </script>
    <script>
$(document).ready(function() {
    getDataHadiah();

    function getDataHadiah() {
        var form = new FormData();
        var settings = {
            async: true,
            crossDomain: true,
            url: "" + rest.url + "/api/give-away",
            method: "GET",
            "headers": {
                "Authorization": rest.token,
            }
        };
        // Tampilkan Hadiah
        $.ajax(settings).done(function(response) {
            // console.log(response.data);
            // return;
            $(".contentH").remove();
            $.each(response.data, function(key, value) {
                // console.log(value.name_product);
                var content =
                    "<div class='col-lg-3 col-md-6 mb-4 contentH'>" +
                    " <div class='card'>" +
                    "<div class='view overlay'>" +
                    "<img class='card-img-top' src='" +
                    rest.url +
                    "/assets/img/" +
                    value.image +
                    "' ></div>" +
                    "<div class='card-body text-center'>" +
                    "<h5 class='card-title'>" +
                    value.gift_name +
                    "</h5>" +
                    "<p class='card-text'>" +
                    value.poin +
                    "</p>" +
                    " <a class='btn btn-success btn-sm' id='tukar' data-id='" +
                    value.id +
                    "'>Tukar</a>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>";
                // console.log(content);
                $("#list_produk").append(content);
            });
        });
    }
    // Tukar
    $(document).on("click", "#tukar", function(e) {
        e.preventDefault();
        Swal.fire({
            title: "Yakin ingin tukar barang ini?",
            icon: "question",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, price it!",
        }).then((result) => {
            if (result.value) {
                var form = new FormData();
                form.append("id_customer", "" + sessionStorage.getItem('id') + "");
                form.append("id_hadiah", "" + $(this).attr('data-id') + "");

                var settings = {
                    "url": "" + rest.url + "/customer/award/add",
                    method: "POST",
                    timeout: 0,
                    headers: {
                        Authorization: rest.token,
                    },
                    processData: false,
                    mimeType: "multipart/form-data",
                    contentType: false,
                    data: form,
                };

                $.ajax(settings).done(function(response) {
                    // console.log(response);
                    // return;
                    var responseText = JSON.parse(response);
                    if (responseText.status == false) {
                        Swal.fire({
                            icon: "error",
                            title: "Gagal",
                            text: responseText.message,
                        });
                    } else {
                        Swal.fire({
                            position: "top-end",
                            icon: "success",
                            title: "Tukar Hadiah berhasil",
                            text: responseText.message,
                            showConfirmButton: false,
                            timer: 1500,
                        });
                        getDataHadiah();
                    }
                });
            }
        });
    });

});
    </script>