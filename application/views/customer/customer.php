    <!-- Heading -->
    <h2 class="my-5 h2 text-center">Daftar Produk</h2>
    <section id="daftar_produk" class="text-center mb-4">
        <div class="row wow fadeIn" id="list_produk">

        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="modalQty" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true" data-backdrop="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">jumlah produk</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="dataid" id="dataid">
                    <input type="number" name="qty" placeholder="qty produk" id="qty">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="close">Close</button>
                    <button type="button" class="btn btn-primary transaksi">Add To Cart</button>
                </div>
            </div>
        </div>
    </div>

    <script>
$(document).ready(function() {
    getDataProduk();

    function getDataProduk() {
        var form = new FormData();
        var settings = {
            async: true,
            crossDomain: true,
            url: "http://localhost/pointsale-server/api/product",
            method: "GET",
            "headers": {
                "Authorization": rest.token,
            }
        };
        // Tampilkan Product
        $.ajax(settings).done(function(response) {

            var list = response.data
            // console.log(list[1].id);
            // return;
            $("#list_produk").html('');
            $.each(list, function(key, value) {
                // console.log(value.name_product);
                var content =
                    "<div class='col-lg-3 col-md-6 mb-4 content'>" +
                    " <div class='card'>" +
                    "<div class='view overlay'>" +
                    "<img class='card-img-top' src='" +
                    rest.url +
                    "/assets/img/" +
                    value.image +
                    "' ></div>" +
                    "<div class='card-body text-center'>" +
                    "<h5 class='card-title'>" +
                    value.name_product +
                    "</h5>" +
                    "<p class='card-text'>Rp." +
                    value.price +
                    ",00</p>" +
                    "<p class='card-text'>" +
                    value.diskon +
                    "%</p>" +
                    "<a class='btn btn-danger btn-sm transaksiP' data-id ='" +
                    value.id +
                    "'data-toggle='modal' data-target='#modalQty' >Add To cart</a>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>";
                // console.log(content);
                $("#list_produk").append(content);
            });
            $('.transaksiP').click(function(e) {
                e.preventDefault()
                $('#modalQty input[name="dataid"]').val($(this).attr("data-id"));
            })
            $(".transaksi").click(function(e) {
                e.preventDefault();
                transaksi();
            });
        });
    };

    function transaksi() {

        Swal.fire({
            title: "Yakin ingin menambahkan barang ini?",
            icon: "question",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, add Cart!",
        }).then((result) => {
            if (result.value) {
                var id = $(this).attr("data-id");
                var form = new FormData();
                form.append("id_product", $("#dataid")
                    .val());
                form.append("qty", $("#qty")
                    .val());
                form.append("id_customer", "" + sessionStorage.getItem(
                    'id') + "");
                var settings = {
                    url: "" + rest.url + "/api/transaksi/add",
                    method: "POST",
                    timeout: 0,
                    headers: {
                        Authorization: rest.token,
                    },
                    processData: false,
                    mimeType: "multipart/form-data",
                    contentType: false,
                    data: form,
                };

                $.ajax(settings).done(function(response) {
                    Swal.fire({
                        position: "top-end",
                        icon: "success",
                        title: "product Added",
                        showConfirmButton: false,
                        timer: 1500,
                    });
                    // $("#modalQty").modal('toggle');
                    // $("#qty")[0].reset();
                    getDataProduk()
                });
            }
        });
    };


});
    </script>
    <script>
try {
    var level = sessionStorage.getItem("level");
    if (level != "customer") {
        window.location.href = "http://localhost/pointsale-client/home";
    }
} catch {
    console.error();
}
    </script>