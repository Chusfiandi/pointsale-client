<div>
    <div>
        <section id="daftar_produk" class="">
            <div class="container">
                <div class="row my-4" id="list_reward">

                </div>
            </div>
        </section>
    </div>
</div>

<script>
try {
    var level = sessionStorage.getItem("level");
    if (level != "customer") {
        window.location.href = "http://localhost/pointsale-client/home";
    }
} catch {
    console.error();
}
</script>

<script>
$(document).ready(function() {
    getDataProduk();

    function getDataProduk() {
        var form = new FormData();
        var settings = {
            async: true,
            crossDomain: true,
            url: "" + rest.url + "/customer/award?id_customer=" + sessionStorage.getItem(
                'id') + "",
            method: "GET",
            "headers": {
                "Authorization": rest.token,
            }
        };
        // Tampilkan Product
        $.ajax(settings).done(function(response) {
            // console.log(response.data);
            // return;
            $(".content").remove();
            $.each(response.data, function(key, value) {
                // console.log(value.name_product);
                var content =
                    "<div class='col-md-2 content'>" +
                    " <div class='card'>" +
                    "<img class='card-img-top' src='" +
                    rest.url +
                    "/assets/img/" +
                    value.image +
                    "' >" +
                    "<div class='card-body'>" +
                    "<h4 class='card-title'>" +
                    value.gift_name +
                    "</h4>" +
                    "<p class='card-text'>didapatkan:" +
                    value.name +
                    "</p>" +
                    "</div>" +
                    "</div>" +
                    "</div>";
                // console.log(content);
                $("#list_reward").append(content);
            });
        });
    }
});
</script>