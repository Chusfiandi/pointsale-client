<script>
try {
    var level = sessionStorage.getItem("level");
    if (level == "customer") {
        window.location.href = "http://localhost/pointsale-client/customer";
    }
} catch {
    console.error();
}
</script>
<div class="container my-5 py-5 z-depth-1">


    <!--Section: Content-->
    <section class="px-md-5 mx-md-5 dark-grey-text text-center text-lg-left">

        <!--Grid row-->
        <div class="row">

            <!--Grid column-->
            <div class="col-lg-6 mb-4 mb-lg-0 d-flex align-items-center justify-content-center">

                <img src="https://mdbootstrap.com/img/Others/food.jpg" class="img-fluid" alt="">

            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-lg-6 mb-4 mb-lg-0">

                <h3 class="font-weight-bold">Call to action</h3>

                <p class="font-weight-bold">That's a very nice subheading</p>

                <p class="text-muted">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Id quam sapiente
                    molestiae
                    numquam quas, voluptates omnis nulla ea odio quia similique corrupti magnam, doloremque laborum.</p>

                <a class="font-weight-bold" href="#">Learn more<i class="fas fa-angle-right ml-2"></i></a>

            </div>
            <!--Grid column-->

        </div>
        <!--Grid row-->


    </section>
    <!--Section: Content-->


</div>
<div class="container my-5">
    <section id="daftar_produk" class="text-center mb-4">
        <div class="row wow fadeIn" id="list_produk">

        </div>
    </section>
</div>
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Sign in</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="md-form mb-5">
                    <i class="fas fa-user prefix grey-text"></i>
                    <input type="text" id="defaultForm-username" class="form-control validate" name="username">
                    <label data-error="wrong" data-success="right" for="defaultForm-email">Your username</label>
                </div>

                <div class="md-form mb-4">
                    <i class="fas fa-lock prefix grey-text"></i>
                    <input type="password" id="defaultForm-pass" class="form-control validate" name="password">
                    <label data-error="wrong" data-success="right" for="defaultForm-pass">Your password</label>
                </div>

            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button class="btn btn-default" id="login">Login</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Sign up</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="md-form mb-5">
                    <i class="fas fa-user prefix grey-text"></i>
                    <input type="text" id="orangeForm-name" class="form-control validate" name="name">
                    <label data-error="wrong" data-success="right" for="orangeForm-name">Your name</label>
                </div>
                <div class="md-form mb-5">
                    <i class="fas fa-envelope prefix grey-text"></i>
                    <input type="text" id="orangeForm-email" class="form-control validate" name="username">
                    <label data-error="wrong" data-success="right" for="orangeForm-email">Your username</label>
                </div>

                <div class="md-form mb-4">
                    <i class="fas fa-lock prefix grey-text"></i>
                    <input type="password" id="orangeForm-pass" class="form-control validate" name="password">
                    <label data-error="wrong" data-success="right" for="orangeForm-pass">Your password</label>
                </div>

            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button class="btn btn-deep-orange" id="register">Sign up</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="notif" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
    data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Maaf Tidak Ada Akun!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Silahkan Register Dulu</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="close">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    getDataProduk();

    function getDataProduk() {
        var form = new FormData();
        var settings = {
            async: true,
            crossDomain: true,
            url: "" + rest.url + "/api/product",
            method: "GET",
            "headers": {
                "Authorization": rest.token,
            }
        };
        // Tampilkan Product
        $.ajax(settings).done(function(response) {
            // console.log(response.data);
            // return;
            $(".content").remove();
            $.each(response.data, function(key, value) {
                // console.log(value.name_product);
                var content =
                    "<div class='col-lg-3 col-md-6 mb-4 content'>" +
                    " <div class='card'>" +
                    "<div class='view overlay'>" +
                    "<img class='card-img-top' src='" +
                    rest.url +
                    "/assets/img/" +
                    value.image +
                    "' ></div>" +
                    "<div class='card-body text-center'>" +
                    "<h5 class='card-title'>" +
                    value.name_product +
                    "</h5>" +
                    "<p class='card-text'>Rp." +
                    value.price +
                    ",00</p>" +
                    "<p class='card-text'>" +
                    value.diskon +
                    "%</p>" +
                    " <a class='btn btn-danger btn-sm' id='adda' data-id ='" +
                    value.id +
                    "'data-toggle='modal' data-target='#notif'>Add To cart</a>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>";
                // console.log(content);
                $("#list_produk").append(content);
            });
        });
    };
    $("#adda").click(function(e) {
        e.preventDefault();
        Swal.fire({
            title: "Register?",
            icon: "question",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Ya, Register Sekarang!",
        }).then((result) => {
            $('#modalRegisterForm').click();
        });
    });


});
</script>