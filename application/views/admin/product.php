<script>

</script>

<div class="row mt-3">
    <button type="button" class="btn btn-primary small" data-toggle="modal" data-target="#insertProduct">
        Tambah <i class="fas fa-plus"></i>
    </button>
</div>

<!-- Modal Insert -->
<div class="modal fade" id="insertProduct" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="card">
                <div class="card-body">
                    <form action="" id="myform">
                        <h4 class="card-title text-center">Form tambah product</h4>
                        <p class="text-center">Silahkan isi form</p>
                        <br>
                        <div class="form-group">
                            <label for="name_product">Nama product</label>
                            <input type="text" class="form-control" name="name_product" id="name_product"
                                placeholder="silahkan isi nama product">
                        </div>
                        <div class="form-group">
                            <label for="price">Harga product</label>
                            <input type="number" class="form-control" name="price" id="price"
                                placeholder="silahkan isi harga product">
                        </div>
                        <div class="form-group">
                            <label for="diskon">Diskon product</label>
                            <input type="number" class="form-control" name="diskon" id="diskon"
                                placeholder="silahkan isi Diskon product">
                        </div>
                        <div class="form-group">
                            <label for="image"> Gambar product</label>
                            <input type="file" class="form-control" name="image" id="image"
                                placeholder="silahkan isi gambar product">
                        </div>
                        <div class="form-group">
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" id="Tproduct" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Update -->
<!-- Modal Edit -->
<div class="modal fade" id="editProduct" tabindex="-1" role="dialog">
    <div class=" modal-dialog" role="document">
        <div class="modal-content">
            <div class="card">
                <div class="card-body">
                    <form action="" id="formEdit">
                        <center>
                            <h4 class="card-title">Form Edit product</h4>
                            <p>Silahkan isi form</p>
                        </center>
                        <br>
                        <div class="form-group">
                            <input type="hidden" class="form-control" name="id" id="Eid">
                            <label for="Ename_product">Nama product</label>
                            <input type="text" class="form-control" name="name_product" id="Ename_product"
                                placeholder="silahkan isi nama product">
                        </div>
                        <div class="form-group">
                            <label for="Eprice">Harga product</label>
                            <input type="text" class="form-control" name="price" id="Eprice"
                                placeholder="silahkan isi harga product">
                        </div>
                        <div class="form-group">
                            <label for="Ediskon">Diskon product</label>
                            <input type="number" class="form-control" name="diskon" id="Ediskon"
                                placeholder="silahkan isi Diskon product">
                        </div>
                        <div class="form-group">
                            <label for="image"> Gambar product</label>
                            <input type="file" class="form-control" name="image" id="Eimage"
                                placeholder="silahkan isi gambar product">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"
                                id="close">Close</button>
                            <button type="submit" id="Esubmit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="mt-3">
    <div class="row">
        <div class="container">
            <table class="table table-hover text-center">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama produk</th>
                        <th scope="col">Harga produk</th>
                        <th scope="col">Diskon produk</th>
                        <th scope="col">Gambar produk</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody id="contentH">

                </tbody>
            </table>
        </div>
    </div>
</section>
<script>
try {
    var level = sessionStorage.getItem("level");
    if (level != "admin") {
        window.location.href = "http://localhost/pointsale-client/admin/auth";
    }
} catch {
    console.error();
}
</script>
<script type="text/javascript" src="<?= base_url('assets/app/product.js'); ?>"></script>