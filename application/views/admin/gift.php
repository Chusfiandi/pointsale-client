<script>

</script>

<div class="row mt-3">
    <button type="button" class="btn btn-primary small" data-toggle="modal" data-target="#insertGift">
        Tambah <i class="fas fa-plus"></i>
    </button>
</div>

<!-- Modal Insert -->
<div class="modal fade" id="insertGift" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="card">
                <div class="card-body">
                    <form action="" id="myform">
                        <h4 class="card-title text-center">Form tambah Hadiah</h4>
                        <p class="text-center">Silahkan isi form</p>
                        <br>
                        <div class="form-group">
                            <label for="gift_name">Nama Hadiah</label>
                            <input type="text" class="form-control" name="gift_name" id="gift_name"
                                placeholder="silahkan isi nama hadiah">
                        </div>
                        <div class="form-group">
                            <label for="poin">Poin</label>
                            <input type="number" class="form-control" name="poin" id="poin"
                                placeholder="silahkan isi poin hadiah">
                        </div>
                        <div class="form-group">
                            <label for="image"> Gambar hadiah</label>
                            <input type="file" class="form-control" name="image" id="image"
                                placeholder="silahkan isi gambar hadiah">
                        </div>
                        <div class="form-group">
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" id="Thadiah" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Update -->
<!-- Modal Edit -->
<div class="modal fade" id="editHadiah" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="card">
                <div class="card-body">
                    <form action="" id="formEdit">
                        <center>
                            <h4 class="card-title">Form Edit hadiah</h4>
                            <p>Silahkan isi form</p>
                        </center>
                        <br>
                        <div class="form-group">
                            <input type="hidden" class="form-control" name="id" id="Eid">
                            <label for="Egift_name">Nama hadiah</label>
                            <input type="text" class="form-control" name="gift_name" id="Egift_name"
                                placeholder="silahkan isi nama hadiah">
                        </div>
                        <div class="form-group">
                            <label for="Epoin">poin hadiah</label>
                            <input type="text" class="form-control" name="poin" id="Epoin"
                                placeholder="silahkan isi poin hadiah">
                        </div>
                        <div class="form-group">
                            <label for="image"> Gambar hadiah</label>
                            <input type="file" class="form-control" name="image" id="Eimage"
                                placeholder="silahkan isi gambar hadiah">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"
                                id="close">Close</button>
                            <button type="submit" id="Esubmit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="mt-3">
    <div class="row">
        <div class="container">
            <table class="table table-hover text-center">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama Hadiah</th>
                        <th scope="col">Poin Hadiah</th>
                        <th scope="col">Gambar Hadiah</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody id="contentH">

                </tbody>
            </table>
        </div>
    </div>
</section>
<script>
try {
    var level = sessionStorage.getItem("level");
    if (level != "admin") {
        window.location.href = "http://localhost/pointsale-client/admin/auth";
    }
} catch {
    console.error();
}
</script>
<script type="text/javascript" src="<?= base_url('assets/app/gift.js'); ?>"></script>