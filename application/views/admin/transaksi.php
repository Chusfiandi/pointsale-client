<section class="mt-3">
    <div class="row">
        <div class="container">
            <table class="table table-hover text-center">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">id customer</th>
                        <th scope="col">id produk</th>
                        <th scope="col">Nama produk</th>
                        <th scope="col">Harga produk</th>
                        <th scope="col">Diskon produk</th>
                        <th scope="col">qty </th>
                        <th scope="col">total</th>
                        <th scope="col">status</th>
                        <th scope="col">tanggal</th>
                    </tr>
                </thead>
                <tbody id="contentH">

                </tbody>
            </table>
        </div>
    </div>
</section>
<script>
try {
    var level = sessionStorage.getItem("level");
    if (level != "admin") {
        window.location.href = "http://localhost/pointsale-client/admin/auth";
    }
} catch {
    console.error();
}
</script>
<script type="text/javascript" src="<?= base_url('assets/app/transaksi.js'); ?>"></script>