<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function index()
    {
        $data['title'] = "Majapahit";
        $this->load->view('template/user/header', $data);
        $this->load->view('template/user/navigation');
        $this->load->view('template/user/corousel');
        $this->load->view('home');
        $this->load->view('template/user/footer');
    }
}