<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customer extends CI_Controller
{
    public function index()
    {
        $data['title'] = "Produk";
        $this->load->view('template/user/header', $data);
        $this->load->view('template/user/navigationC');
        $this->load->view('customer/customer');
        $this->load->view('template/user/footer');
    }
    public function myreward()
    {
        $data['title'] = "MyReward";
        $this->load->view('template/user/header', $data);
        $this->load->view('template/user/navigationC');
        $this->load->view('customer/myreward');
        $this->load->view('template/user/footer');
    }
    // public function transaki()
    // {
    //     $data['title'] = "transaksi";
    //     $this->load->view('template/user/header', $data);
    //     $this->load->view('template/user/navigationC');
    //     $this->load->view('customer/transaksi');
    //     $this->load->view('template/user/footer');
    // }
    public function exchange()
    {
        $data['title'] = "TukarHadiah";
        $this->load->view('template/user/header', $data);
        $this->load->view('template/user/navigationC');
        $this->load->view('customer/exchange');
        $this->load->view('template/user/footer');
    }
    public function histori()
    {
        $data['title'] = "Histori Transaksi";
        $this->load->view('template/user/header', $data);
        $this->load->view('template/user/navigationC');
        $this->load->view('customer/histori');
        $this->load->view('template/user/footer');
    }
}