<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

    public function index()
    {
        $data['title'] = "product";
        $this->load->view('template/admin/header', $data);
        $this->load->view('template/admin/navigation');
        $this->load->view('admin/product');
        $this->load->view('template/admin/footer');
    }
    public function gift()
    {
        $data['title'] = "Hadiah";
        $this->load->view('template/admin/header', $data);
        $this->load->view('template/admin/navigation');
        $this->load->view('admin/gift');
        $this->load->view('template/admin/footer');
    }
    public function alltransaksi()
    {
        $data['title'] = "Transaksi";
        $this->load->view('template/admin/header', $data);
        $this->load->view('template/admin/navigation');
        $this->load->view('admin/transaksi');
        $this->load->view('template/admin/footer');
    }
}