$(document).ready(function () {
	getDataTransaksi();

	function getDataTransaksi() {
		var form = new FormData();
		var settings = {
			url: "" + rest.url + "/api/transaksi/allhistori",
			method: "GET",
			timeout: 0,
			headers: {
				Authorization: rest.token,
			},
		};

		$.ajax(settings).done(function (response) {
			dataP = response.data;
			// console.log(response.data);
			// return;
			$(".remove").remove();
			$.each(dataP, function (i, data) {
				$("#contentH").append(
					`
	                <tr class="remove">
	                    <th scope="row">${i + 1}</th>
	                    <td>${data.id_customer}</td>
						<td>${data.id_product}</td>
                        <td>${data.name_product}</td>
                        <td>${data.price_product}</td>
                        <td>${data.diskon}%</td>
                        <td>${data.qty}</td>
                        <td>${data.total}</td>
                        <td>${data.status}</td>
                        <td>${data.created_at}</td>
	                </tr>
	            `
				);
			});
		});
	}
});
