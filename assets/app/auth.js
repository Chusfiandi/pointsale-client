$(document).ready(function () {
	$("#register").click(function (e) {
		// onsole.log("oke");
		// return;c
		e.preventDefault();
		var form = new FormData();
		form.append(
			"username",
			$('#modalRegisterForm input[name = "username"]').val()
		);
		form.append("name", $('#modalRegisterForm input[name = "name"]').val());
		form.append(
			"password",
			$('#modalRegisterForm input[name = "password"]').val()
		);

		var settings = {
			url: "" + rest.url + "/customer/register",
			method: "POST",
			timeout: 0,
			processData: false,
			mimeType: "multipart/form-data",
			contentType: false,
			data: form,
		};

		$.ajax(settings).done(function (response) {
			var responseText = JSON.parse(response);
			if (responseText.status == false) {
				Swal.fire({
					icon: "error",
					title: "Gagal",
					text: responseText.message,
				});
			} else {
				const Toast = Swal.mixin({
					toast: true,
					position: "top-end",
					showConfirmButton: false,
					timer: 3000,
					timerProgressBar: true,
					onOpen: (toast) => {
						toast.addEventListener("mouseenter", Swal.stopTimer);
						toast.addEventListener("mouseleave", Swal.resumeTimer);
					},
				});

				Toast.fire({
					icon: "success",
					title: "Register in successfully",
				});
			}
			// $("#modalRegisterForm ").toggle();
			// return false;
		});
	});

	// login
	$("#login").click(function (e) {
		e.preventDefault();
		var form = new FormData();
		form.append(
			"username",
			$('#modalLoginForm input[name = "username"]').val()
		);
		form.append(
			"password",
			$('#modalLoginForm input[name = "password"]').val()
		);

		var settings = {
			url: "" + rest.url + "/customer/login",
			method: "POST",
			timeout: 0,
			processData: false,
			mimeType: "multipart/form-data",
			contentType: false,
			data: form,
		};

		$.ajax(settings).done(function (response) {
			var responseText = JSON.parse(response);
			var data = responseText.data;
			if (responseText.status == false) {
				Swal.fire({
					icon: "error",
					title: "Gagal",
					text: responseText.message,
				});
			} else {
				// console.log(data);
				// return;
				const Toast = Swal.mixin({
					toast: true,
					position: "top-end",
					showConfirmButton: false,
					timer: 3000,
					timerProgressBar: true,
					onOpen: (toast) => {
						toast.addEventListener("mouseenter", Swal.stopTimer);
						toast.addEventListener("mouseleave", Swal.resumeTimer);
					},
				});
				Toast.fire({
					icon: "success",
					title: "Login in successfully",
				});
				setTimeout(function () {
					sessionStorage.setItem("id", data.id_customer);
					sessionStorage.setItem("username", data.username);
					sessionStorage.setItem("level", data.level);
					window.location.href = "http://localhost/pointsale-client/customer";
				}, 3000);
			}
		});
	});
	// logout
	$("#logout").click(function (e) {
		sessionStorage.removeItem("id");
		sessionStorage.removeItem("username");
		sessionStorage.removeItem("level");
		const Toast = Swal.mixin({
			toast: true,
			position: "top-end",
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			onOpen: (toast) => {
				toast.addEventListener("mouseenter", Swal.stopTimer);
				toast.addEventListener("mouseleave", Swal.resumeTimer);
			},
		});

		Toast.fire({
			icon: "success",
			title: "Logout in successfully",
		});
		window.location.href = "http://localhost/pointsale-client/home";
	});
});
