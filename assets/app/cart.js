$(document).ready(function () {
	$(document).on("click", "#cart", function () {
		getCart();
	});

	kupoin();
	function getCart() {
		var form = new FormData();

		var settings = {
			url:
				"" +
				rest.url +
				"/api/transaksi?id_customer=" +
				sessionStorage.getItem("id") +
				"",
			dataType: "json",
			method: "GET",
			timeout: 0,
			headers: {
				Authorization: rest.token,
			},
			processData: false,
			mimeType: "multipart/form-data",
			contentType: false,
			data: form,
		};

		$.ajax(settings).done(function (response) {
			if (response.status == false) {
				Swal.fire({
					icon: "info",
					title: "Not Found",
					text: response.message,
				});
				let content = "";
				$.each(response.data, function (i, pro) {
					content += isicart(pro, i + 1);
				});
				$("#cartisi").html(content);
			} else {
				let content = "";
				$.each(response.data, function (i, pro) {
					content += isicart(pro, i + 1);
				});
				$("#cartisi").html(content);
				$(".checkout").click(function (e) {
					e.preventDefault();
					checkout();
				});
			}
		});
	}

	function isicart(isi, i) {
		return `<tr>
		<th scope="row">${i}</th>
        <td>${isi.name_product}</td>
        <td>${isi.price_product}</td>
        <td>${isi.qty}</td>
		<td>${isi.total}</td>
		<td><a class="checkout" data-id="${isi.id}"><i class="fas fa-money-bill-alt green-text mr-3"></i></a></td>
    </tr>`;
	}
	// checkout
	function checkout() {
		var id = $(".checkout").attr("data-id");
		var form = new FormData();
		form.append("id_customer", "" + sessionStorage.getItem("id") + "");
		form.append("id_transaksi", "" + id + "");

		var settings = {
			url: "" + rest.url + "/api/transaksi/checkoutA",
			method: "POST",
			timeout: 0,
			headers: {
				Authorization: rest.token,
			},
			processData: false,
			mimeType: "multipart/form-data",
			contentType: false,
			data: form,
		};
		// checkout
		$.ajax(settings).done(function (response) {
			var responseText = JSON.parse(response);
			// console.log(data);
			// return;
			const Toast = Swal.mixin({
				toast: true,
				position: "top-end",
				showConfirmButton: false,
				timer: 3000,
				timerProgressBar: true,
				onOpen: (toast) => {
					toast.addEventListener("mouseenter", Swal.stopTimer);
					toast.addEventListener("mouseleave", Swal.resumeTimer);
				},
			});
			Toast.fire({
				icon: "success",
				title: "checkout succes in successfully",
				text: responseText.message,
			});
			// $("#modalCard").reset();
		});
	}
	function kupoin() {
		$(document).on("click", "#kupoin", function () {
			var form = new FormData();
			var settings = {
				url:
					"" +
					rest.url +
					"/customer/point?id_customer=" +
					sessionStorage.getItem("id") +
					"",
				dataType: "json",
				method: "GET",
				timeout: 0,
				headers: {
					Authorization: rest.token,
				},
				processData: false,
				mimeType: "multipart/form-data",
				contentType: false,
				data: form,
			};
			$.ajax(settings).done(function (response) {
				if (response.status == false) {
					Swal.fire({
						icon: "info",
						title: "Not Found",
						text: response.message,
					});
				} else {
					var dataP = response.data.point;
					// $("p:first").text(dataP);
					Swal.fire({
						icon: "info",
						title: "point anda:",
						text: dataP,
					});
				}
			});
			// kupoin();
		});
	}
});
