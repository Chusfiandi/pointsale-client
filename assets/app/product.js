$(document).ready(function () {
	// var token = $("body").data("token");

	getDataProduct();

	function getDataProduct() {
		var form = new FormData();
		var settings = {
			url: "" + rest.url + "/api/product",
			method: "GET",
			timeout: 0,
			headers: {
				Authorization: rest.token,
			},
		};

		$.ajax(settings).done(function (response) {
			dataP = response.data;
			// console.log(response.data);
			// return;
			$(".remove").remove();
			$.each(dataP, function (i, data) {
				$("#contentH").append(
					`
	                <tr class="remove">
	                    <th scope="row">${i + 1}</th>
	                    <td>${data.name_product}</td>
						<td>${data.price}</td>
						<td>${data.diskon}%</td>
	                    <td><img class="imgC" src="` +
						rest.url +
						`/assets/img/` +
						data.image +
						`"></td>
	                    <td>
	                    	<button type="button" class="btn btn-primary btn-sm btn-ubah" id="ubahP" name="btn-ubah" data-toggle="modal" data-target="#editProduct" data-id=` +
						data.id +
						`>Ubah</button>
	                    	<a href="" class="btn btn-danger btn-sm btn-hapus" data-id=` +
						data.id +
						` id="hapusP" name="btn-hapus">Hapus</a>
	                    </td>
	                </tr>
	            `
				);
			});
		});
	}
	// Tambah Hadiah
	$("#Tproduct").click(function (e) {
		e.preventDefault();
		Swal.fire({
			title: "Yakin ingin menambahkan barang ini?",
			icon: "question",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, Upload Data!",
		}).then((result) => {
			if (result.value) {
				var file = $("#image")[0].files[0];
				var form = new FormData();
				form.append("name_product", $("#name_product").val());
				form.append("diskon", $("#diskon").val());
				form.append("image", file);
				form.append("price", $("#price").val());

				var settings = {
					url: "" + rest.url + "/api/product/add",
					method: "POST",
					timeout: 0,
					headers: {
						Authorization: rest.token,
					},
					processData: false,
					mimeType: "multipart/form-data",
					contentType: false,
					data: form,
				};

				$.ajax(settings).done(function (response) {
					var responseText = JSON.parse(response);
					if (responseText.status == false) {
						Swal.fire({
							icon: "error",
							title: "Gagal",
							text: responseText.message,
						});
					} else {
						Swal.fire({
							position: "top-end",
							icon: "success",
							title: "Product Added",
							showConfirmButton: false,
							timer: 1500,
						});
					}
					$("#myform")[0].reset();
					getDataProduct();
				});
			}
		});
	});

	$(document).on("click", "#hapusP", function (e) {
		e.preventDefault();
		const swalWithBootstrapButtons = Swal.mixin({
			customClass: {
				confirmButton: "btn btn-success",
				cancelButton: "btn btn-danger",
			},
			buttonsStyling: false,
		});

		swalWithBootstrapButtons
			.fire({
				title: "Are you sure?",
				text: "You won't be able to revert this!",
				icon: "warning",
				showCancelButton: true,
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No, cancel!",
				reverseButtons: true,
			})
			.then((result) => {
				if (result.value) {
					var settings = {
						async: true,
						crossDomain: true,
						url:
							"" +
							rest.url +
							"/api/product/" +
							$(this).attr("data-id") +
							"/delete",
						method: "DELETE",
						headers: {
							authorization: rest.token,
						},
					};
					$.ajax(settings).done(function (response) {
						getDataProduct();
						swalWithBootstrapButtons.fire(
							"Deleted!",
							"Your gift has been deleted.",
							"success"
						);
					});
				} else if (
					/* Read more about handling dismissals below */
					result.dismiss === Swal.DismissReason.cancel
				) {
					swalWithBootstrapButtons.fire(
						"Cancelled",
						"Your imaginary product is safe :)",
						"error"
					);
				}
				// location.reload()
			});
	});
	// ubah product
	$(document).on("click", "#ubahP", function () {
		var form = new FormData();
		form.append("id", $(this).attr("data-id"));

		var settings = {
			url: "" + rest.url + "/api/product?id=" + $(this).attr("data-id"),
			method: "GET",
			timeout: 0,
			headers: {
				Authorization: rest.token,
			},
			processData: false,
			mimeType: "multipart/form-data",
			contentType: false,
			data: form,
		};

		$.ajax(settings).done(function (response) {
			var data = JSON.parse(response);
			var dataH = data.data;
			$('#formEdit input[name =  "name_product"]').val(dataH.name_product);
			$('#formEdit input[name =  "price"]').val(dataH.price);
			$('#formEdit input[name =  "diskon"]').val(dataH.diskon);
			$('#formEdit input[name =  "id"]').val(dataH.id);
		});
	});

	$("#Esubmit").click(function (e) {
		e.preventDefault();
		Swal.fire({
			title: "Yakin ingin mengubah barang ini?",
			icon: "question",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, Upload Data!",
		}).then((result) => {
			if (result.value) {
				var file = $("#Eimage")[0].files[0];
				var form = new FormData();
				form.append("_method", "PUT");
				form.append("id", $("#Eid").val());
				form.append("name_product", $("#Ename_product").val());
				form.append("image", file);
				form.append("price", $("#Eprice").val());
				form.append("diskon", $("#Ediskon").val());

				var settings = {
					async: true,
					crossDomain: true,
					url: "" + rest.url + "/api/product/update",
					method: "POST",
					headers: {
						authorization: rest.token,
					},
					processData: false,
					contentType: false,
					mimeType: "multipart/form-data",
					data: form,
				};

				$.ajax(settings).done(function (response) {
					getDataProduct();
					Swal.fire({
						position: "top-end",
						icon: "success",
						title: "product Added",
						showConfirmButton: false,
						// timer: 1500,
					});
					// $("#editProduct").modal("toggle");
				});
			}
		});
	});
});
