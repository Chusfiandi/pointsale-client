$(document).ready(function () {
	// var token = $("body").data("token");

	getDataHadiah();

	function getDataHadiah() {
		var form = new FormData();
		var settings = {
			url: "" + rest.url + "/api/give-away",
			method: "GET",
			timeout: 0,
			headers: {
				Authorization: rest.token,
			},
		};

		$.ajax(settings).done(function (response) {
			dataH = response.data;
			$(".remove").remove();
			$.each(dataH, function (i, data) {
				$("#contentH").append(
					`
	                <tr class="remove">
	                    <th scope="row">${i + 1}</th>
	                    <td>${data.gift_name}</td>
	                    <td>${data.poin}</td>
	                    <td><img class="imgC" src="` +
						rest.url +
						`/assets/img/` +
						data.image +
						`"></td>
	                    <td>
	                    	<button type="button" class="btn btn-primary btn-sm btn-ubah" id="ubahH" name="btn-ubah" data-toggle="modal" data-target="#editHadiah" data-id=` +
						data.id +
						`>Ubah</button>
	                    	<a href="" class="btn btn-danger btn-sm btn-hapus" data-id=` +
						data.id +
						` id="hapusH" name="btn-hapus">Hapus</a>
	                    </td>
	                </tr>
	            `
				);
			});
		});
	}
	// Tambah Hadiah
	$("#Thadiah").click(function (e) {
		e.preventDefault();
		Swal.fire({
			title: "Yakin ingin menambahkan barang ini?",
			icon: "question",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, Upload Data!",
		}).then((result) => {
			if (result.value) {
				var file = $("#image")[0].files[0];
				var form = new FormData();
				form.append("gift_name", $("#gift_name").val());
				form.append("image", file);
				form.append("poin", $("#poin").val());

				var settings = {
					url: "" + rest.url + "/api/give-away/add",
					method: "POST",
					timeout: 0,
					headers: {
						Authorization: rest.token,
					},
					processData: false,
					mimeType: "multipart/form-data",
					contentType: false,
					data: form,
				};

				$.ajax(settings).done(function (response) {
					// console.log(response);
					// return;
					Swal.fire({
						position: "top-end",
						icon: "success",
						title: "gift Added",
						showConfirmButton: false,
						timer: 1500,
					});
					$("#myform")[0].reset();
					getDataHadiah();
				});
			}
		});
	});

	$(document).on("click", "#hapusH", function (e) {
		e.preventDefault();
		const swalWithBootstrapButtons = Swal.mixin({
			customClass: {
				confirmButton: "btn btn-success",
				cancelButton: "btn btn-danger",
			},
			buttonsStyling: false,
		});

		swalWithBootstrapButtons
			.fire({
				title: "Are you sure?",
				text: "You won't be able to revert this!",
				icon: "warning",
				showCancelButton: true,
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No, cancel!",
				reverseButtons: true,
			})
			.then((result) => {
				if (result.value) {
					var settings = {
						async: true,
						crossDomain: true,
						url:
							"" +
							rest.url +
							"/api/give-away/" +
							$(this).attr("data-id") +
							"/delete",
						method: "DELETE",
						headers: {
							authorization: rest.token,
						},
					};
					$.ajax(settings).done(function (response) {
						getDataHadiah();
						swalWithBootstrapButtons.fire(
							"Deleted!",
							"Your gift has been deleted.",
							"success"
						);
					});
				} else if (
					/* Read more about handling dismissals below */
					result.dismiss === Swal.DismissReason.cancel
				) {
					swalWithBootstrapButtons.fire(
						"Cancelled",
						"Your imaginary gift is safe :)",
						"error"
					);
				}
				// location.reload()
			});
	});
	// ubah Hadiah
	$(document).on("click", "#ubahH", function () {
		var form = new FormData();
		form.append("id", $(this).attr("data-id"));

		var settings = {
			url: "" + rest.url + "/api/give-away?id=" + $(this).attr("data-id"),
			method: "GET",
			timeout: 0,
			headers: {
				Authorization: rest.token,
			},
			processData: false,
			mimeType: "multipart/form-data",
			contentType: false,
			data: form,
		};

		$.ajax(settings).done(function (response) {
			var data = JSON.parse(response);
			var dataH = data.data;
			$('#formEdit input[name =  "gift_name"]').val(dataH.gift_name);
			$('#formEdit input[name =  "poin"]').val(dataH.poin);
			$('#formEdit input[name =  "id"]').val(dataH.id);
		});
	});

	$("#Esubmit").click(function (e) {
		e.preventDefault();
		Swal.fire({
			title: "Yakin ingin mengubah barang ini?",
			icon: "question",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, Upload Data!",
		}).then((result) => {
			if (result.value) {
				var file = $("#Eimage")[0].files[0];
				var form = new FormData();
				form.append("_method", "PUT");
				form.append("id", $("#Eid").val());
				form.append("gift_name", $("#Egift_name").val());
				form.append("image", file);
				form.append("poin", $("#Epoin").val());

				var settings = {
					async: true,
					crossDomain: true,
					url: "" + rest.url + "/api/give-away/update",
					method: "POST",
					headers: {
						authorization: rest.token,
					},
					processData: false,
					contentType: false,
					mimeType: "multipart/form-data",
					data: form,
				};

				$.ajax(settings).done(function (response) {
					var responseText = JSON.parse(response);
					if (responseText.status == false) {
						Swal.fire({
							icon: "error",
							title: "Gagal",
							text: responseText.message,
						});
					} else {
						Swal.fire({
							position: "top-end",
							icon: "success",
							title: "gift Added",
							showConfirmButton: false,
							timer: 1500,
						});
					}
					getDataHadiah();
				});
			}
		});
	});
});
